Bash script that searches for tags that contain git commits that reference a
provided phrase.

**Installation**

Copy it to your $PATH

**Usage**

In git repository execute `git find <phrase>` to find out which tags contain
commit messages that reference it

**Example**

```bash
➜  tokio git:(master) git find future
tokio-0.2.161121a8eb sync: ensure Mutex, RwLock, and Semaphore futures are Send + Sync (#2375)
tokio-0.2.1264e75ad1 time: Add comment about cancelation of timed out futures (#2206)
tokio-0.2.12be832f20 util: add futures-io/tokio::io compatibility layer (#2117)
tokio-0.2.115bf06f2b future: provide try_join! macro (#2169)
tokio-0.2.117079bcd6 future: provide join! macro (#2158)
(...)
```
